<?php
namespace app;

use think\swoole\response\File as sFile;

class File extends sFile
{
	public function __construct($file, string $contentDisposition = null, bool $autoEtag = false, bool $autoLastModified = true, bool $autoContentType = true)
    {
    	parent::__construct($file, $contentDisposition = null, $autoEtag = false, $autoLastModified = true, $autoContentType = true);
    }

	public function setAutoContentType()
    {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);

        $filename = $this->file->getPathname();
        $mimeType = finfo_file($finfo, $filename);

        $ext = pathinfo($filename, PATHINFO_EXTENSION); 
        switch($ext) {
            case 'css':
                $mimeType = 'text/css';
                break;
            case 'js':
                $mimeType = 'application/javascript';
            default:
                break;
        }

        if($mimeType) {
            $this->header['Content-Type'] = $mimeType;
        }
    }
}