<?php
// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

namespace app\jobs;

use think\facade\Log;
use think\swoole\Websocket;
use app\webscoket\Manager;
use think\queue\Job;

/**
 * 消息发送队列
 * Class UniPush
 * @package app\jobs
 */
class swooleTask
{
    public function fire(Job $job, $data)
    {
        if (method_exists($this, $data['type'])) {
            $this->{$data['type']}($data['data']);
        } else {
            Log::error('任务执行失败,' . $data['type'] . '方法不存在');
        }
        $job->delete();
    }

    public function message(array $data)
    {
        /** @var Server $server */
        $server = app()->make(Websocket::class);
        $userId = is_array($data['user_id']) ? $data['user_id'] : [$data['user_id']];
        $except = $data['except'] ?? [];
        if (!count($userId) && $data['type'] != 'user') {
            $fds = Manager::userFd($data['type']);
            foreach ($fds as $fd) {
                if (!in_array($fd, $except)) {
                    $server->to($fd)->push(json_encode($data['data']));
                }
            }
        } else {
            foreach ($userId as $id) {
                $fds = Manager::userFd($data['type'], $id);
                foreach ($fds as $fd) {
                    if (!in_array($fd, $except)) {
                        $server->to($fd)->push(json_encode($data['data']));
                    }
                }
            }
        }
    }
}
